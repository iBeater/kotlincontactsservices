package com.example.amartineza.contacts

/**
 * Created by amartineza on 4/4/2018.
 */

import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

fun CircleImageView.loafFromUrl(url: String?){
    Picasso.with(this.context).load(url).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).into(this)
}