package com.example.amartineza.contacts.callbacks

import com.example.amartineza.contacts.models.Contact

/**
 * Created by amartineza on 4/3/2018.
 */
interface ContactsCallback: BaseCallback {
    fun onGetContactsResponse (contacts: List<Contact>)

    fun onFilterContactsComplete(contacts: List<Contact>)
}