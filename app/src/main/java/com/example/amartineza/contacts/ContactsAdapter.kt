package com.example.amartineza.contacts

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.amartineza.contacts.models.Contact
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by amartineza on 4/3/2018.
 */
class ContactsAdapter(private val context: Context, private val contacts: MutableList<Contact>) :
        RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>() {

    private val layoutInflater: LayoutInflater by lazy { LayoutInflater.from(this.context) }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ContactsViewHolder {
        val view = this.layoutInflater.inflate(R.layout.contact_layout_item, parent, false)
        return ContactsViewHolder(view)
    }

    override fun getItemCount(): Int = this.contacts.size

    override fun onBindViewHolder(holder: ContactsViewHolder?, position: Int) {
        val contact = this.contacts[position]
        holder?.profileImage?.loafFromUrl(contact.picture?.medium)
        holder?.nameTextView?.text = contact.name?.fullName
        holder?.emailTextView?.text = contact.email
        holder?.cellPhoneTextView?.text = contact.cell
    }

    fun updateContacts(contacts: List<Contact>) {
        this.contacts.clear()
        this.contacts.addAll(contacts)
        this.notifyDataSetChanged()
    }

    class ContactsViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val profileImage: CircleImageView? = itemView?.findViewById(R.id.profile_image)
        val nameTextView: TextView? = itemView?.findViewById(R.id.text_view_contact_name)
        val emailTextView: TextView? = itemView?.findViewById(R.id.text_view_email)
        val cellPhoneTextView: TextView? = itemView?.findViewById(R.id.text_view_cell_phone_number)
    }

}