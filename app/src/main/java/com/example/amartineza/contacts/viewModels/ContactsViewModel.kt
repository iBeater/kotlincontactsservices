package com.example.amartineza.contacts.viewModels

import com.example.amartineza.contacts.models.Contact

/**
 * Created by amartineza on 4/3/2018.
 */
interface ContactsViewModel: BaseViewModel {

    fun setProgressVisibility(progressVisibility: Int,
                              emptyMessageVisibility: Int,
                              listVisibility: Int)

    fun setRefreshing(refresh: Boolean)

    fun displayContacts(Contacts: List<Contact>)
}