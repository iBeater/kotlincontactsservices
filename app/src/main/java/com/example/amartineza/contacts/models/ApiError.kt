package com.example.amartineza.contacts.models

import com.google.gson.annotations.SerializedName

/**
 * Created by amartineza on 4/3/2018.
 */
data class ApiError(
        @SerializedName("error") private val _error: String?) {

    val error: String
        get() = this._error ?: ""
}