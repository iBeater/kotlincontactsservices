package com.example.amartineza.contacts.models

import com.google.gson.annotations.SerializedName

/**
 * Created by amartineza on 4/3/2018.
 */
data class ResultResponse<out T>(
        @SerializedName("results") private val _results:List<T>?){

    val result: List<T>
        get() = this._results ?: emptyList()
}