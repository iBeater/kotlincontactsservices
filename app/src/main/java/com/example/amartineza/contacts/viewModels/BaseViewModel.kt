package com.example.amartineza.contacts.viewModels

import com.example.amartineza.contacts.models.ApiError

/**
 * Created by amartineza on 4/3/2018.
 */
interface BaseViewModel {
    fun displayApíError(apiError: ApiError)
}