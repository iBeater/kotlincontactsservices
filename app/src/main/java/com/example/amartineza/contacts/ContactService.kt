package com.example.amartineza.contacts

import com.example.amartineza.contacts.models.Contact
import com.example.amartineza.contacts.models.ResultResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by amartineza on 4/3/2018.
 */
interface ContactService {

    @GET("api/")
    fun getContacts(@Query("results") numberOfResults: Int,
                    @Query("gender") gender: String): Call<ResultResponse<Contact>>

}