package com.example.amartineza.contacts.callbacks

import com.example.amartineza.contacts.models.ApiError

/**
 * Created by amartineza on 4/3/2018.
 */
interface BaseCallback {
    fun onError(apiError: ApiError)
}