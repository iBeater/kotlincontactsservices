package com.example.amartineza.contacts

import android.view.View
import com.example.amartineza.contacts.callbacks.ContactsCallback
import com.example.amartineza.contacts.models.ApiError
import com.example.amartineza.contacts.models.Contact
import com.example.amartineza.contacts.viewModels.ContactsViewModel

/**
 * Created by amartineza on 4/3/2018.
 */
class ContactsPresenter(private val contactsViewModel: ContactsViewModel) : ContactsCallback {

    private val interactor: ContactsInteractor by lazy { ContactsInteractor(this) }

    private var contacts: List<Contact> = emptyList()

    fun updateContacts (numberOfContacts: Int, gender: String = ""){
        this.contactsViewModel.setProgressVisibility(
                View.VISIBLE,
                View.INVISIBLE,
                View.VISIBLE
        )
        this.interactor.getContacts(numberOfContacts, gender)
    }

    fun refreshContacts (numberOfContacts: Int,gender: String = ""){
        this.contactsViewModel.setProgressVisibility(
                View.INVISIBLE,
                View.INVISIBLE,
                View.VISIBLE
        )
        this.interactor.getContacts(numberOfContacts, gender)
    }

    fun filterContacts(query: String){
        this.interactor.filterContacts(query, this.contacts)
    }

    override fun onGetContactsResponse(contacts: List<Contact>) {
        this.contacts = contacts
        this.contactsViewModel.setProgressVisibility(
                View.INVISIBLE,
                if(this.contacts.isEmpty()) View.VISIBLE else View.INVISIBLE,
                if(this.contacts.isEmpty()) View.INVISIBLE else View.VISIBLE
        )
        this.contactsViewModel.setRefreshing(false)
        this.contactsViewModel.displayContacts(this.contacts)
    }

    override fun onFilterContactsComplete(contacts: List<Contact>) {
        this.contactsViewModel.displayContacts(contacts)
    }

    override fun onError(apiError: ApiError) {
        this.contactsViewModel.setProgressVisibility(
                View.INVISIBLE,
                if(this.contacts.isEmpty()) View.VISIBLE else View.INVISIBLE,
                if(this.contacts.isEmpty()) View.INVISIBLE else View.VISIBLE
        )
        this.contactsViewModel.setRefreshing(false)
        this.contactsViewModel.displayApíError(apiError)
    }
}