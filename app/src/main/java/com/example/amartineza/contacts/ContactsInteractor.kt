package com.example.amartineza.contacts

import com.example.amartineza.contacts.callbacks.ContactsCallback
import com.example.amartineza.contacts.models.ApiError
import com.example.amartineza.contacts.models.Contact
import com.example.amartineza.contacts.models.ResultResponse

/**
 * Created by amartineza on 4/3/2018.
 */
class ContactsInteractor(private val contactsCallback: ContactsCallback) {
    private val contactsServices: ContactService by lazy {
        ServiceGenerator.retrofit.create(ContactService::class.java)
    }

    fun getContacts(numberOfContacts: Int, gender: String = "") {
        this.contactsServices.getContacts(numberOfContacts, gender).enqueue(object : RetrofitCallback<ResultResponse<Contact>>() {
            override fun onResponseSuccess(response: ResultResponse<Contact>) {
                contactsCallback.onGetContactsResponse(response.result)
            }

            override fun onError(apiError: ApiError) {
                contactsCallback.onError(apiError)
            }
        })
    }

    fun filterContacts(query: String, contacts: List<Contact>) {
        if(query.isBlank()){
            contactsCallback.onFilterContactsComplete(contacts)
        }else{
            val filterContacts = contacts.filter { contact ->
                val name = contact.name?.fullName ?: ""
                val email = contact.email
                val cellPhone = contact.cell
                name.contains(query, true) or email.contains(query, true) or cellPhone.contains(query, true)
            }
            val resultContacts = if(filterContacts.isEmpty()) contacts else filterContacts
            contactsCallback.onFilterContactsComplete(resultContacts)
        }
    }

}