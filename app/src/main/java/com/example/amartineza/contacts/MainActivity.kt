package com.example.amartineza.contacts

import android.app.SearchManager
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.SearchView
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import com.example.amartineza.contacts.models.ApiError
import com.example.amartineza.contacts.models.Contact
import com.example.amartineza.contacts.viewModels.ContactsViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ContactsViewModel {

    private val presenter: ContactsPresenter by lazy { ContactsPresenter(this) }
    private val contactsAdapter: ContactsAdapter by lazy { ContactsAdapter(this, mutableListOf()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view_contacts.adapter = contactsAdapter

        this.refresh_layout.setOnRefreshListener {
            this.presenter.refreshContacts(30)
            this.toggle_button.isChecked = false
        }

        this.udpdate_button.setOnClickListener {
            this.presenter.updateContacts(30)
        }

        this.presenter.updateContacts(30)

        this.toggle_button.setOnClickListener {
            if(this.toggle_button.isChecked){
                this.presenter.updateContacts(30, "female")
            }else{
                this.presenter.updateContacts(30, "male")
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menuInflater.inflate(R.menu.main, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as? SearchManager
        val searchView = menu?.findItem(R.id.search)?.actionView as? SearchView
        searchView?.setSearchableInfo(searchManager?.getSearchableInfo(this.componentName))
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextChange(newText: String?): Boolean {
                val query = newText ?:""
                presenter.filterContacts(query)
                return  query.isNotBlank()
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                val newQuery = query ?:""
                presenter.filterContacts(newQuery)
                return  newQuery.isNotBlank()
            }
        })

        return true
    }

    override fun setProgressVisibility(progressVisibility: Int, emptyMessageVisibility: Int, listVisibility: Int) {
        this.progress_container.visibility = progressVisibility
        this.empty_message_container.visibility = emptyMessageVisibility
        this.refresh_layout.visibility = listVisibility
    }

    override fun setRefreshing(refresh: Boolean) {
        this.refresh_layout.isRefreshing = refresh
    }

    override fun displayContacts(Contacts: List<Contact>) {
        this.contactsAdapter.updateContacts(Contacts)
    }

    override fun displayApíError(apiError: ApiError) {
        val snack = Snackbar.make(this.main_container, apiError.error, Snackbar.LENGTH_INDEFINITE)
        snack.setAction(android.R.string.ok){
            snack.dismiss()
        }
        snack.show()
    }

}
